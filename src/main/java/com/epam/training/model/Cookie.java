package com.epam.training.model;

public class Cookie {
    private String name;
    private int weight;

    public Cookie() {
        name ="Default cookie";
        weight = 100;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
