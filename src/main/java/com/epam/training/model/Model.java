package com.epam.training.model;

public interface Model {
    String getName();
    int getWeight();
    void setWeight(int weight);
    void setName(String name);
    String myMethod(String a, int... args);
    String myMethod(String a);

}
