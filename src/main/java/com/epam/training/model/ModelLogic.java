package com.epam.training.model;


import com.epam.training.annotations.CustomFieldAnnotation;
import com.epam.training.annotations.CustomMethodAnnotation;
import com.epam.training.annotations.CustomObjectAnnotation;

import java.util.Arrays;

@CustomObjectAnnotation(name = "biscuits")
public class ModelLogic implements Model {

    @CustomFieldAnnotation(name = "chocolateCookie")
    Cookie cookie;

    public ModelLogic() {
        cookie = new Cookie();
    }

    @Override
    @CustomMethodAnnotation(name = "nameGetter")
    public String getName() {
        return cookie.getName();
    }

    @Override
    @CustomMethodAnnotation(name = "nameSetter")
    public void setName(String name) {

    }

    @Override
    @CustomMethodAnnotation(name = "weightGetter")
    public int getWeight() {
        return cookie.getWeight();
    }

    @Override
    @CustomMethodAnnotation(name = "weightSetter")
    public void setWeight(int weight) {
        cookie.setWeight(weight);
    }

    @Override
    public String myMethod(String a, int... args){
        StringBuilder result = new StringBuilder("");
        result.append(a);
        Arrays.stream(args).forEach(arg -> result.append(arg));
        return result.toString();
    }

    @Override
    public String myMethod(String a){
        StringBuilder result = new StringBuilder("");
        result.append(a);
        return result.toString();
    }

    @CustomMethodAnnotation(name = "cookieGetter")
    public Cookie getCookie() {
        return cookie;
    }

}
