package com.epam.training.view;

import com.epam.training.controller.BiscuitsController;
import com.epam.training.controller.Controller;
import com.epam.training.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {
    static final Logger logger = LogManager.getLogger(View.class.getName());
    private Map<String, String> menu;
    private Map<String, Runnable> methodsMenu;
    private Scanner scanner;
    Controller biscuitController;

    Locale locale;
    ResourceBundle bundle;

    public View() {
        biscuitController = new BiscuitsController();
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
    }

    public void run() {
        String keyMenu;
        do {
            generateMainMenu();
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        logger.info("MENU:");
        for (String option : menu.values()) {
            logger.info(option);
        }
        logger.info(">>> ");
    }

    private void generateMainMenu() {
        menu.clear();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("Q", bundle.getString("Q"));

        methodsMenu.clear();
        methodsMenu.put("1", this::i18nEnglish);
        methodsMenu.put("2", this::i18nUkrainian);
        methodsMenu.put("3", this::pressButton3);
        outputMenu();
    }

    private void i18nEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu");
    }

    private void i18nUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
    }

    private void pressButton3() {
        biscuitController.getAnnotations();
        biscuitController.getMethodAndInvoke();
        try {
            biscuitController.setValue();

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }



}
