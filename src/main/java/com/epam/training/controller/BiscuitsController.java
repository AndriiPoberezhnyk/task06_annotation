package com.epam.training.controller;

import com.epam.training.model.Cookie;
import com.epam.training.model.Model;
import com.epam.training.model.ModelLogic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class BiscuitsController implements Controller {
    static final Logger logger = LogManager.getLogger(BiscuitsController.class.getName());
    ModelLogic cookieLogic;
    Class c;

    public BiscuitsController() {
        this.cookieLogic = new ModelLogic();
        c = cookieLogic.getClass();
    }

    @Override
    public void getAnnotations() {
        logger.info("Annotations");
        Arrays.stream(c.getAnnotations()).forEach(logger::info);
        Arrays.stream(c.getDeclaredFields()).forEach(logger::info);

        Arrays.stream(c.getDeclaredMethods()).forEach(method ->{
            logger.info(method.getName());
            Arrays.stream(method.getDeclaredAnnotations())
                    .forEach(annotation -> logger.info(annotation.toString()));
        });
    }

    public void setValue() throws NoSuchFieldException, IllegalAccessException {
        Field field = c.getDeclaredFields()[0];
        logger.info(c.getDeclaredFields().length);
        field.setAccessible(true);
        field.set(cookieLogic, 123);
        logger.info(field.get(cookieLogic));
    }

    @Override
    public void getMethodAndInvoke(){
        // Object[] obj={"hello"}; //for myMethod(String str)
        Object[] obj={"hello", new int[]{1,2,3,4}};
        Class<?> params[] = new Class[obj.length];
        for (int i = 0; i < obj.length; i++) {
            if (obj[i] instanceof int[]) {
                params[i] = int[].class;
            } else if (obj[i] instanceof String) {
                params[i] = String.class;
            }
        }

        try {
            Method method = c.getDeclaredMethod("getName");
            logger.warn(method.getName());
            String methodReturn = (String) method.invoke(cookieLogic);
            logger.warn(methodReturn);
            Arrays.stream(c.getDeclaredMethods()).forEach(logger::info);
            method = c.getDeclaredMethod("myMethod", params);
            logger.warn(method.getName());
            logger.warn(method.getParameterCount());
            Object _instance = c.newInstance();
            methodReturn = (String) method.invoke(_instance, obj);
            logger.warn(methodReturn);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Cookie getCookie() {
        return cookieLogic.getCookie();
    }

    @Override
    public void changeCookieName(String name) {
        cookieLogic.setName(name);
    }

    @Override
    public void bite(int weight) {
        cookieLogic.setWeight(cookieLogic.getWeight()-weight);
    }

}
