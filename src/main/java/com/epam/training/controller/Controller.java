package com.epam.training.controller;

import com.epam.training.model.Cookie;

public interface Controller {

    Cookie getCookie();
    void changeCookieName(String name);
    void bite(int weight);
    void getAnnotations();
    void setValue() throws NoSuchFieldException, IllegalAccessException;
    void getMethodAndInvoke();
}
